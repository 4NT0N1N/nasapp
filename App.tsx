/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import 'react-native-gesture-handler';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
} from 'react-native';

// Navigation 
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/screens/HomeScreen';
import PictureScreen from './src/screens/PictureScreen';


const NasaStack = createStackNavigator()
function MyNasaStack() {
  return (
    <NasaStack.Navigator headerMode="none" initialRouteName="Home">
      <NasaStack.Screen name="Home" component={HomeScreen} />
      <NasaStack.Screen name="Picture" component={PictureScreen} />
    </NasaStack.Navigator>
  )
}

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1, backgroundColor: "#000" }}>
        <NavigationContainer>
            <MyNasaStack />
        </NavigationContainer>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  
});

export default App;
