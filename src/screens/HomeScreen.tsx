import React, { useState, useEffect } from 'react';
import { StyleSheet, View, StatusBar, FlatList, TouchableOpacity, Animated } from 'react-native';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

// Swipe component
import SwipeCards from 'react-native-swipe-cards-deck';

import {PictureComponent} from '../components/PictureComponent';
import SmallPictureComponent from '../components/SmallPictureComponent';

// Axios
import axios from 'axios';
// Moment.js
import moment from 'moment';
import 'moment/locale/fr-ca';
import { WINDOW_WIDTH } from '../constants';

// Set date
moment.locale('fr-ca');

// Import API Service
import { Picture } from '../types/types';
import { toPicture } from '../utils';

const HomeScreen = (props: any) => {
    const [picturesInitialState, setPicturesInitialState] = useState<Picture[]>([]);
    const [pictures, setPictures] = useState<Picture[]>([]);
    const [visible, setVisibility] = useState(false);

    // create animation component
    const AnimatedTouchableOpacity = Animated.createAnimatedComponent(TouchableOpacity);
    
    // data call 
    const getPictures = async() => {
        var pictures: Array<Picture> = [];

        for(let index = 0; index < 7; index++)
        {
            var date = moment().subtract(index, 'days').format('l');
            const reponse = await axios.get(`https://api.nasa.gov/planetary/apod?api_key=PPd24FdF8kZFd9EMgvbvNBAAwNh5Ds3NZI1gPwYO&date=${date}`);
            const picture = toPicture(reponse.data);
            pictures.push(picture);
        }    

        setPicturesInitialState([...pictures]);
        setPictures([...pictures]);
    }

    // Request API
    useEffect(() => {
        getPictures();
    }, [])

    // Re-order des images après le clic sur une petite photo
    const orderPictures = (index: number) => {
        let array = picturesInitialState;
        let elements:Picture[] = [];
        elements = array.splice(0, index);
        elements.map((element) => {
            array.push(element);
        });
        // Update
        setPictures([...array]);
    }

    // animation
    const opacity = () => {
        const opacity = new Animated.Value(0);
        Animated.timing(opacity, {toValue: 1, duration: 500, useNativeDriver: true}).start();
        return opacity
    }

    const value = opacity();

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <LinearGradient colors={['#000', '#000', "transparent"]} style={styles.linear}>
                {
                visible ?
                <Animated.View style={{ opacity: value }}>
                    <TouchableOpacity onPress={() => setVisibility(!visible)} style={styles.icon}>
                        <Icon name="angle-up" color="#fff" size={40} type="font-awesome"/>
                    </TouchableOpacity>
                    <FlatList 
                        keyExtractor={(item, index) => item.title}
                        data={pictures}
                        style={styles.flatlist}
                        contentContainerStyle={{justifyContent: 'space-around', alignItems: 'center'}}
                        horizontal
                        renderItem ={({item, index}) => {
                            return (
                            <TouchableOpacity onPress={() => {orderPictures(index)}} onLongPress={() => {props.navigation.navigate("Picture", { picture: item })}}>
                                <SmallPictureComponent picture={item} />
                            </TouchableOpacity>)
                            
                        }}
                    />
                </Animated.View>
                :
                <AnimatedTouchableOpacity onPress={() => setVisibility(!visible)} style={[styles.icon, {opacity: value}]}>
                    <Icon name="angle-down" color="#fff" size={40} type="font-awesome"/>
                </AnimatedTouchableOpacity>
                }
            </LinearGradient>
            
            {pictures.length !== 0 ? 
                <SwipeCards
                    cards={pictures}
                    renderCard={(picture: Picture) => {
                        return(<PictureComponent picture={picture} />)
                       }}
                    keyExtractor={(item:Picture) => item.title}
                    loop
                    showYup={false}
                    showNope={false}
                    handleYup={() => {return true}}
                />
            : null}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#000"
    },
    linear: {
        position: 'absolute',
        height: 120,
        zIndex: 100
    },
    flatlist: {
        maxHeight: 120,
        width: WINDOW_WIDTH,
    },
    icon: {
        height: 30,
        width: WINDOW_WIDTH,
    },
});

export default HomeScreen