import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet, View, StatusBar, ImageBackground, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';

const PictureScreen = (props: any) => {
  const { picture } = props.route.params


  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1 }}>
        <ImageBackground source={{ uri: picture.url }} style={styles.image} resizeMode="cover">
          <TouchableOpacity onPress={() => {props.navigation.navigate("Home")}} style={styles.icon}>
            <Icon name="angle-left" color="#fff" size={50} type="font-awesome"/>
          </TouchableOpacity>
        </ImageBackground>
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  icon: {
    margin: 20
  },
  image: {
    height: "100%",
    width: "100%",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },
});

export default PictureScreen