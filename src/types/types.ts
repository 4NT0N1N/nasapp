export type Picture = {
    title: string;
    url: string;
    description: string;
    date: Date;
}
