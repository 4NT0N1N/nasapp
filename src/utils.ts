import { Picture } from "./types/types"

export const toPicture = (data: any) => {
    const picture: Picture = {
        title: data.title,
        description: data.explanation,
        url: data.url,
        date: data.date,
    }
    return picture
}