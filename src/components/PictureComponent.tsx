import React, { useEffect, useState } from 'react'
import { ImageBackground, StyleSheet, Text, View, ScrollView, TouchableOpacity, Animated } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Picture } from '../types/types';



type Props = {
    picture: Picture
}

export const PictureComponent: React.FC<Props> = ({ picture }) => {
    const [visible, setVisibility] = useState(false);

    const opacity = () => {
        // animation
        const opacity = new Animated.Value(0);

        Animated.timing(opacity, {toValue: 1, duration: 700, useNativeDriver: true}).start();

        return opacity
    }

    const value = opacity();


    return (
        <View style={[styles.container, {}]}>
            <ImageBackground source={{ uri: picture.url }} style={styles.image}>
            <LinearGradient colors={['transparent', '#000']} style={styles.linearGradient}>
                <ScrollView contentContainerStyle={styles.description}>
                    <TouchableOpacity onPress={() => setVisibility(!visible)}>
                        <Animated.Text style={[styles.title, {opacity: value}]} numberOfLines={visible ? 3 : 2} ellipsizeMode='tail'>{picture.title}</Animated.Text>
                    </TouchableOpacity>
                    {visible ? <Animated.Text style={[styles.info, {opacity: value}]} ellipsizeMode='tail'>{picture.description}</Animated.Text> : null}
                </ScrollView>
            </LinearGradient>
            </ImageBackground>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        minWidth: "100%",
        backgroundColor: "#000",
        justifyContent: "center",
        alignItems: "center"
    },
    image: {
        height: "100%",
        width: "100%",
    },
    linearGradient: {
        flex: 1,
      },
    info: {
        fontSize: 16,
        fontWeight: '600',
        color: "#fff",
    },
    title: {
        fontSize: 40,
        fontWeight: 'bold',
        color: "#fff",
    },
    description: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems:  "center",
        margin: 20
    }
});
