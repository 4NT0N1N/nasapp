import React from 'react'
import { ImageBackground, StyleSheet, Text, View, ScrollView, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Picture } from '../types/types';

type Props = {
    picture: Picture
}

const SmallPictureComponent: React.FC<Props> = ({ picture }) => {
    return (
        <View style={styles.container}>
            <Image source={{ uri: picture.url }} style={styles.image} resizeMode="cover" />
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        height: 75,
        width: 75,
        borderColor: "#fff",
        borderWidth: 1,
        margin: 10,
        borderRadius: 10

    },
    image: {
        height: "100%",
        width: "100%",
        borderRadius: 10
    },
});

export default SmallPictureComponent;
